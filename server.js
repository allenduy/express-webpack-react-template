const express = require('express');
const path = require('path');
const ip = require('ip');
const app = express();

// directory to be served
app.use(express.static(path.join(__dirname, 'www')));

// allow routing without path error by directing to index output
app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, 'www/index.html'));
});

const serverLocal = app.listen(3000, "127.0.0.1", function() {
  const host = serverLocal.address().address;
  const port = serverLocal.address().port;
  console.log('[App] listening on localhost at http://%s:%s', host, port);
});

const serverNetwork = app.listen(3000, ip.address(), function() {
  const host = serverNetwork.address().address;
  const port = serverNetwork.address().port;
  console.log('[App] listening on network at http://%s:%s', host, port);
});
