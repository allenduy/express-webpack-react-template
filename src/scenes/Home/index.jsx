import React from 'react';

import './style.scss';
import Button from 'components/Button';

class Home extends React.Component {
  render() {
    return (
      <div styleName="container">
        <div className="content">
          <span className="title">
            it works&#9787;
            <span className="caption">now go have a party</span>
          </span>
          <Button to="example" label="example route" />
          <Button href="https://google.com" label="external" />
        </div>
        <div className="background"></div>
      </div>
    );
  }
}

export default Home;
