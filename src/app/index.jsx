import React from 'react';
import {

  BrowserRouter,
  Switch,
  Route,
  Redirect

} from 'react-router-dom';

import 'font-awesome/css/font-awesome.css';
// font awesome icons

import 'whatwg-fetch';
// for fetching http endpoints

import './style.scss';
import Home from 'scenes/Home';
import Example from 'scenes/Example';

class App extends React.Component {
  render() {
    return (  /* wrapper/container */
      <BrowserRouter>
        <Switch>

          <Route exact path="/" component={ Home } />
          <Route path="/example" component={ Example } />

          {
            /*
              <Redirect from="/express-webpack-react-template" to="/" />
              replace from path to repo name for publishing to:
                USER.gitlab.io/REPO_NAME
            */
          }

        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
